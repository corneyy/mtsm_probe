#include "protocol.hpp"
#include "mcast_controller.hpp"

#include <iostream>
#include <tuple>
#include <functional>
#include <queue>

#include <poll.h>
#include <signal.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <string.h>

using namespace mtsm_probe;

void sigterm(int signo) {
}

class Sender {
  using Buffer = std::vector<uint8_t>;

  std::queue<Buffer> buffers;

 public:
  sockaddr_in addr;
  socklen_t socklen;

  Sender(): socklen{sizeof(addr)} {
  }

  uint8_t* buffer(size_t len) {
    buffers.emplace(len);
    return buffers.back().data();
  }

  int sendto(int socket) {
    int sum{0};
    while(!buffers.empty()) {
      auto ret = ::sendto(socket, buffers.front().data(), buffers.front().size(), 0, (sockaddr*)&addr, socklen);
      sum+=ret;
      buffers.pop();
    }
    return sum;
  }
};

void prepare_signals(sigset_t*);
uint64_t alive_id();

int main(int argc, char *argv[]) {
  if(argc<2) {
    std::cout<<"Use: "<<argv[0]<<" <port>"<<std::endl;
    return -1;
  }

  unsigned long port=std::stoul(argv[1]);
  uint16_t cmd_port=port;

  if(cmd_port!=port) {
    std::cout<<"Incorrect port"<<std::endl;
    return -1;
  }

  sigset_t oldmask;
  prepare_signals(&oldmask);

  auto cmd_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  sockaddr_in addr{AF_INET, htons(cmd_port), htonl(INADDR_ANY)};

  if(bind(cmd_socket, (const sockaddr*)&addr, sizeof(addr))!=0) {
    std::cout<<"Bind error: "<<strerror(errno)<<std::endl;
    return -1;
  }

  McastController mcast{};
  Sender sender{};

  Protocol<Sender> protocol{
    alive_id(),
    sender,
    std::function<bool(uint32_t, uint16_t)>{
      [&mcast](uint32_t addr, uint16_t port) -> bool {
        mcast.check(addr, port);
        return true;
      }
    },
    std::function<void(uint32_t, uint16_t, const Protocol<Sender>::McastStateCallFront&)>{
      [&mcast](uint32_t addr, uint16_t port, const Protocol<Sender>::McastStateCallFront& cf) {
        auto state=mcast.state(addr, port);
        cf({
          state.buffer_overflow_, state.sync_error, state.error_count, state.tei, state.scrambled,
          state.pids_num, state.packet_count, state.null_count
        });
      }
    },
    std::function<bool(uint32_t, uint16_t)>{
      [&mcast](uint32_t addr, uint16_t port) -> bool {
        mcast.stop(addr, port);
        return true;
      }
    },
    std::function<void()>{
      [&mcast]() {
        mcast.stop();
      }
    }
  };

  const int PPOLL_TIMEOUT_SEC{5};

  std::vector<pollfd> fds;

  for(;;) {
    struct timespec timeout {
      PPOLL_TIMEOUT_SEC, 0
    };

    if(mcast.is_modified()) {
      fds.clear();
      fds.emplace_back(pollfd{cmd_socket, POLLIN});
      for(auto s: mcast.sockets()) fds.emplace_back(pollfd{s, POLLIN});
    }

    auto ret = ppoll(fds.data(), fds.size(), &timeout, &oldmask);
    if(ret>0) {
      for(size_t i=1; i<fds.size(); ++i) {
        if(fds[i].revents&POLLIN) {
          mcast.pollin(fds[i].fd);
        }
      }
      if(fds[0].revents&POLLIN) {
        uint8_t buff[100];
        auto len=recvfrom(cmd_socket, buff, sizeof(buff), 0, (sockaddr*)&sender.addr, &sender.socklen);
        if(len>0) {
          protocol.incoming(std::make_pair(buff, len));
        }
      }
    } else if(0==ret) {
    } else break;

    protocol.tick();
    sender.sendto(cmd_socket);
  }

  close(cmd_socket);

  return 0;
}

void prepare_signals(sigset_t* oldmask_p) {
  struct sigaction s;
  s.sa_handler = sigterm;
  sigemptyset(&s.sa_mask);
  s.sa_flags = 0;
  sigaction(SIGTERM, &s, NULL);

  sigset_t sigmask;
  sigemptyset(&sigmask);
  sigaddset(&sigmask, SIGTERM);

  sigprocmask(SIG_BLOCK, &sigmask, oldmask_p);
}

uint64_t alive_id() {
  return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}
