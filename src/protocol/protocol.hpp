#ifndef PROTOCOL_HPP_INCLUDED
#define PROTOCOL_HPP_INCLUDED

#include "msgunpacker.hpp"
#include "dissector.hpp"

#include <utility>
#include <tuple>
#include <functional>
#include <chrono>
#include <unordered_map>

#include <netinet/in.h>
#include <endian.h>

namespace mtsm_probe {
using namespace std::placeholders;

template<class Sender> class Protocol {
 public:
  using McastStateCallFront = std::function<void(std::initializer_list<uint32_t>)>;

 private:
  using Buffer = std::pair<uint8_t*, size_t>;
  using Clock = std::chrono::steady_clock;
  using IntervalUnit = std::chrono::seconds;
  const IntervalUnit STATE_INTERVAL{10};
  const IntervalUnit HOST_TIMEOUT{60};

  using CheckCallback = std::function<bool(uint32_t, uint16_t)>;
  using McastStateCallback = std::function<void(uint32_t, uint16_t, McastStateCallFront&)>;
  using StopCheckCallback = std::function<bool(uint32_t, uint16_t)>;
  using ResetCallback = std::function<void()>;

  const uint8_t SIGN_SRC[4] = {'M','T','S','M'};
  const uint32_t SIGN = *((uint32_t*)(SIGN_SRC));

  enum MessageType:uint8_t {KEEP_ALIVE = 0x1, CHECK, STATUS, STOP_CHECK};

  //SIGN, sender's packet num, peer's packet num, message type
  using Header = MsgUnpacker<uint32_t, uint64_t, uint64_t, uint8_t>;
  Header header_unpacker_;

  // Dup's counter
  MsgUnpacker<uint64_t, uint32_t> keep_alive_;
  // mcast addr, port
  MsgUnpacker<uint32_t, uint16_t> check_;
  // mcast addr, port
  MsgUnpacker<uint32_t, uint16_t> stop_check_;

  Dissector msg_dissector_;

  Sender& sender_;
  CheckCallback check_cb_;
  McastStateCallback mcast_state_cb_;
  StopCheckCallback stop_check_cb_;
  ResetCallback reset_cb_;

  uint64_t last_packet_num_;
  uint64_t peer_packet_num_;
  uint64_t incoming_peer_packet_num_;
  uint32_t dup_counter_;

  using Mcast = std::pair<uint32_t, uint16_t>;
  struct HashMcast {
    size_t operator()(const Mcast &mcast ) const {
      return std::hash<uint32_t>()(mcast.first) ^ std::hash<uint16_t>()(mcast.second);
    }
  };

  std::unordered_map<Mcast, std::chrono::time_point<Clock>, HashMcast> mcast2last_;

  uint64_t alive_id_;
  uint64_t host_alive_id_;

  std::chrono::time_point<Clock> last_cmd_time_;

 public:
  Protocol(uint64_t alive_id, Sender& sender, CheckCallback check_cb, McastStateCallback mcast_state_cb,
           StopCheckCallback stop_check_cb, ResetCallback reset_cb):
    header_unpacker_ {std::bind(&Protocol::unpack_header, this, _1)},
    keep_alive_      {std::bind(&Protocol::keep_alive,    this, _1)},
    check_           {std::bind(&Protocol::check,         this, _1)},
    stop_check_      {std::bind(&Protocol::stop_check,    this, _1)},
    msg_dissector_   {std::make_tuple(KEEP_ALIVE, keep_alive_), std::make_tuple(CHECK, check_),
                      std::make_tuple(STOP_CHECK, stop_check_)},
    sender_{sender},
    check_cb_{check_cb},
    mcast_state_cb_{mcast_state_cb},
    stop_check_cb_{stop_check_cb},
    reset_cb_{reset_cb},
    last_packet_num_{0},
    peer_packet_num_{0},
    dup_counter_{0},
    alive_id_{alive_id},
    host_alive_id_{0},
    last_cmd_time_{Clock::now()} {
  }

  void incoming(Buffer buffer) {
    header_unpacker_.unpack(buffer);
  }

  void tick() {
    auto now=Clock::now();

    if(std::chrono::duration_cast<IntervalUnit>(now - last_cmd_time_) >= HOST_TIMEOUT) {
      reset();
      return;
    }

    for(auto& mcast: mcast2last_) {
      IntervalUnit delta = std::chrono::duration_cast<IntervalUnit>(now - mcast.second);
      if(delta>=STATE_INTERVAL) {
        McastStateCallFront fun {
          [this, &mcast](std::initializer_list<uint32_t> list) {
            send_mcast_state(mcast.first.first, mcast.first.second, list);
          }
        };
        mcast_state_cb_(mcast.first.first, mcast.first.second, fun);
        mcast.second+=delta;
      }
    }
  }

 private:
  void unpack_header(const Header::Message& msg) {
    if(SIGN!=msg.get<0>()) return;

    last_cmd_time_=Clock::now();

    incoming_peer_packet_num_=ntohll(msg.get<1>());
    if(incoming_peer_packet_num_<=peer_packet_num_) {
        ++dup_counter_;
        if(msg.get<3>()==KEEP_ALIVE) msg_dissector_.dissect(KEEP_ALIVE, msg.rest() );
    } else {
      peer_packet_num_=incoming_peer_packet_num_;
      msg_dissector_.dissect(msg.get<3>(), msg.rest() );
    }
  }

  void keep_alive(const MsgUnpacker<uint64_t, uint32_t>::Message& msg) {
    auto new_host_alive_id=msg.get<0>();
    if(0==host_alive_id_) host_alive_id_=new_host_alive_id;
    else if(host_alive_id_!=new_host_alive_id) {
      host_alive_id_=new_host_alive_id;
      reset();
    }

    Buffer rest=begin_pack(KEEP_ALIVE, keep_alive_.len());

    rest=keep_alive_.pack(rest, ntohll(alive_id_), htonl(dup_counter_));

    end_pack(rest);
  }

  void reset() {
    if(incoming_peer_packet_num_<=peer_packet_num_ && dup_counter_>0) --dup_counter_;
    peer_packet_num_=incoming_peer_packet_num_;
    mcast2last_.clear();
    reset_cb_();
  }

  void check(const MsgUnpacker<uint32_t, uint16_t>::Message& msg) {
    auto mcast=std::make_pair(ntohl(msg.get<0>()), ntohs(msg.get<1>()));

    if(mcast2last_.find(mcast)!=mcast2last_.end()) return;

    if(check_cb_(ntohl(msg.get<0>()), ntohs(msg.get<1>()))) {
      mcast2last_.emplace(mcast, Clock::now());
    }
  }

  void stop_check(const MsgUnpacker<uint32_t, uint16_t>::Message& msg) {
    if(stop_check_cb_(ntohl(msg.get<0>()), ntohs(msg.get<1>()))) {
      mcast2last_.erase(std::make_pair(ntohl(msg.get<0>()), ntohs(msg.get<1>())));

      Buffer rest=begin_pack(STOP_CHECK, stop_check_.len());

      rest=stop_check_.pack(rest, msg.get<0>(), msg.get<1>());

      end_pack(rest);
    }
  }

  // field's num and order is user depended
  void send_mcast_state(uint32_t addr, uint16_t port, std::initializer_list<uint32_t> list) {
    if(list.size()==0) return;

    Buffer rest=begin_pack(STATUS, check_.len()+list.size()*sizeof(uint32_t));

    rest=check_.pack(rest, htonl(addr), htons(port));

    uint32_t* buff_p{static_cast<uint32_t*>((void*)rest.first)};
    for(uint32_t f: list) {
      *buff_p++ = htonl(f);
    }

    end_pack(rest);
  }

  Buffer begin_pack(MessageType type, size_t msg_len) {
    size_t len=header_unpacker_.len()+msg_len;
    uint8_t* buff=sender_.buffer(len);
    Buffer rest=header_unpacker_.pack(std::make_pair(buff, len), SIGN, htonll(++last_packet_num_), htonll(peer_packet_num_), type);
    return rest;
  }

  void end_pack(Buffer& buff) { // just in case
  }

  inline uint64_t ntohll(uint64_t u64) {
    return be64toh(u64);
  }

  inline uint64_t htonll(uint64_t u64) {
    return htobe64(u64);
  }
};

}
#endif // PROTOCOL_HPP_INCLUDED
