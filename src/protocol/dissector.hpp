#ifndef DISSECTOR_HPP_INCLUDED
#define DISSECTOR_HPP_INCLUDED

#include <array>
#include <memory>
#include <algorithm>
#include <utility>

namespace mtsm_probe {

class Dissector {
  using Buffer = std::pair<uint8_t*, size_t>;

  class Command{
 public:
  virtual ~Command(){}
  virtual void process(Buffer){}
};

template<typename U> class UnpackCommand: public Command {
 public:
  U unpacker_;
  UnpackCommand(U unpacker):unpacker_{unpacker}{}
  ~UnpackCommand() override {}
  virtual void process(Buffer buffer){unpacker_.unpack(buffer);}
};

  using CommandPtr = std::unique_ptr<Command>;
  std::array<CommandPtr, 256> commands_;

 public:
  template<typename... Commands> Dissector(Commands... commands) {
    std::generate(commands_.begin(),commands_.end(),[](){return CommandPtr{new Command};});
    add(commands...);
  }

  void dissect(uint8_t idx, Buffer buffer){commands_[idx]->process(buffer);}

 private:
  template<typename C, typename... Commands> void add(C cmd, Commands... commands) {
    addCommand(std::get<0>(cmd), std::get<1>(cmd));
    add(commands...);
  }

  template<typename C> void add(C cmd) {
    addCommand(std::get<0>(cmd), std::get<1>(cmd));
  }

  template<typename U> void addCommand(uint8_t idx, U unpacker) {
    commands_[idx] = CommandPtr{new UnpackCommand<U>{unpacker}};
  }
};

}
#endif // DISSECTOR_HPP_INCLUDED
