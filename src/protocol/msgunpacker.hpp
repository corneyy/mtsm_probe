#ifndef MSGUNPACKER_HPP_INCLUDED
#define MSGUNPACKER_HPP_INCLUDED

#include "typeopers.hpp"

#include <type_traits>
#include <utility>
#include <cstdint>
#include <functional>

namespace mtsm_probe {

template<typename... Types> class MsgUnpacker {
  using Buffer = std::pair<uint8_t*, size_t>;

 public:
  class Message {
    Buffer buffer_;
   public:
    Message(Buffer buffer):buffer_{buffer} {}

    template<size_t idx> TypeSelect<idx, Types...> get() const {
      size_t s=offset<idx, Types...>::calc();

      return *(static_cast<typename std::add_pointer<TypeSelect<idx, Types...>>::type>((void*)(buffer_.first+s)));
    }

    Buffer rest() const {
      return std::make_pair(buffer_.first+total<(sizeof...(Types))-1, Types...>::calc(), buffer_.second-total<(sizeof...(Types))-1, Types...>::calc());
    }
  };

 private:
  using Callback = std::function<void(const Message&)>;
  Callback callback_;

 public:
  MsgUnpacker(Callback cb):callback_{cb} {}

  void unpack(Buffer buffer) const {
    if(buffer.second<total<(sizeof...(Types))-1, Types...>::calc()) return; //TODO violation of preconditions processing
    Message msg{buffer};
    callback_(msg);
  }

  Buffer pack(Buffer buffer, Types... args ) const {
    if(buffer.second<total<(sizeof...(Types))-1, Types...>::calc()) return buffer; //TODO violation of preconditions processing
    return pack_p(buffer, args...);
  }

  constexpr static size_t len() {
    return total<(sizeof...(Types))-1, Types...>::calc();
  }

 private:
  template<typename T, typename... Args> Buffer pack_p(Buffer buffer, T value, Args... args ) const {
    *(static_cast<T*>((void*)buffer.first))=value;
    return pack_p(std::make_pair(buffer.first+sizeof(T), buffer.second-sizeof(T)), args...);
  }

  Buffer pack_p(Buffer buffer) const {
    return buffer;
  }
};

}
#endif // MSGUNPACKER_HPP_INCLUDED
